#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <ctime>
#include <random>

#include "RandomMapGenerator.h"

void randomMapGenerator(){
	std::random_device r;
	std::seed_seq seed{ r() };
	std::mt19937 eng{ seed };
	std::uniform_int_distribution<> random(0, 1);
	std::uniform_int_distribution<> wall(9, 12);
	std::uniform_int_distribution<> ground(0, 8);

	srand(time(NULL));
	int columnCounter = 1;
	int map[100][100];
	std::ofstream myfile("randomMap.map");
	if (myfile.is_open()) {
		columnCounter = 1;
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				if(i == 0 || i == 99 || j == 0 || j == 99) {
					map[i][j] = wall(eng);
				}
				else {
					//Ograniczenie tworzenia scian kolo siebie przy scianach granicznych mapy w celu unikni�cia podw�jnej �ciany granicznej
					if (map[i][j - 1] >=9 && map[i][j-1] <= 12 && j > 1 && i == 1) {
						map[i][j] = ground(eng);
					}
					else if(map[i][j - 1] >= 9 && map[i][j - 1] <= 12 && j > 1 && i == 98){
						map[i][j] = ground(eng);
					} else if(map[i-1][j] >= 9 && map[i-1][j] <= 12 && i > 1 && j == 1){
						map[i][j] = ground(eng);
					}
					else if (map[i-1][j] >= 9 && map[i-1][j] <= 12 && i > 1 && j == 98) {
						map[i][j] = ground(eng);
					}
					else {
						if (i > 1 && map[i - 1][j] >= 0 && map[i - 1][j] <= 8 && map[i - 1][j + 1] >= 9 && map[i - 1][j + 1] <= 12 && j < 98) {
							map[i][j] = ground(eng);
						}
						else if (i > 1 && map[i - 1][j] >= 0 && map[i - 1][j] <= 8 && map[i - 1][j + 1] >= 0 && map[i - 1][j + 1] <= 8 && map[i][j - 1] >= 0 && map[i][j - 1] <= 8) {
							map[i][j] = wall(eng);
						}
						else {
							if (random(eng) == 1) {
								map[i][j] = ground(eng);
							}
							else {
								map[i][j] = wall(eng);
							}
						}
					}
				}
				if (map[i][j] < 10) {
					myfile << "0" << map[i][j] << " ";
				}
				else {
					myfile << map[i][j] << " ";
				}
				columnCounter++;
				if (columnCounter == 101) {
					columnCounter = 1;
					myfile << "\n";
				}
			}
		}

		myfile.close();
	}
}