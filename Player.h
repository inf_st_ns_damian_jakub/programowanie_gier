#pragma once
#include "Globals.h"
#include "Tile.h"
#include "LTexture.h"
#include "Enemy.h"
#include <vector>

class Player {
public:
	static const int PLAYER_WIDTH = 32;
	static const int PLAYER_HEIGHT = 32;

	Player();
	~Player();

	void handleEvent(SDL_Event &event);
	void move(Tile *tiles[], std::vector<Enemy *> enemy_container);
	void setCamera( SDL_Rect &camera );
	void render(SDL_Rect& camera);
	bool loadMedia();
	void free();
	bool wallCollision(SDL_Rect playerBox, Tile *tiles[]);
	bool enemyCollision(SDL_Rect playerBox, std::vector<Enemy *> enemy_container);
	void enemyClicked(std::vector<Enemy*> enemy_container);
	SDL_Rect getBox();

private:
	Tile *tile;
	LTexture playerTexture;
	SDL_Texture* playerImage;
	SDL_Rect playerBox;
	SDL_Rect collision;
	int speed;
	int baseSpeed = 32;
	int velX, velY;
	int distanceX, distanceY;
	bool click;
	int mouseX, mouseY;
	std::vector<Enemy *>::iterator enemy_iterator;
};