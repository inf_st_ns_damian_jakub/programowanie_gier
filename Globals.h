#pragma once
#include "GameStates.h"
#include "GameState.h"
#include <SDL.h>

extern int stateID;
extern int nextState;

extern GameState* currentState;
extern SDL_Renderer* gRenderer;
extern SDL_Event event;
extern SDL_Rect camera;
