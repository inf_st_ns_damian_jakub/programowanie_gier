#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>

#include "Constants.h"
#include "MainFunctions.h"
#include "Globals.h"
using namespace std;

SDL_Window* gWindow = NULL;

bool initSDL() {
	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		cout << "SDL nie zainicjowany! SDL_Error:" << SDL_GetError() << endl;
		success = false;
	}
	else {
		gWindow = SDL_CreateWindow("Forgotten Labyrinth", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, Constants::SCREEN_W, Constants::SCREEN_H, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			cout << "Nie mozna utworzyc okna! SDL_Error: " << SDL_GetError() << endl;
			success = false;
		}
		else {
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL) {
				cout << "Nie mozna utworzyc renderera! SDL Error: " << SDL_GetError() << endl;
				success = false;
			}
			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags) & imgFlags)) {
				cout << "Nie mozna wczytac obrazu! SDL_Error: " << SDL_GetError() << endl;
			}
		}
	}

	return success;
}

void closeGame() {
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	IMG_Quit();
	SDL_Quit();
}