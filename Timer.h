#pragma once

class Timer {
public:
	Timer();
	void start();
	void stop();
	void pause();
	void unpause();
	int getTicks();

	bool isStarted();
	bool isPaused();
private:
	int startTicks;
	int pausedTicks;
	bool paused;
	bool started;
};