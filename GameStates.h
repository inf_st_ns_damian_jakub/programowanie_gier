#pragma once

enum GameStates {
	STATE_NULL,
	STATE_MENU,
	STATE_LEVEL,
	STATE_EXIT,
};