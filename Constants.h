#pragma once
namespace Constants {
	const int SCREEN_W = 1280;
	const int SCREEN_H = 720;

	const int LEVEL_W = 3200;
	const int LEVEL_H = 3200;

	const int FPS = 20;

	const int TILE_WIDTH = 32;
	const int TILE_HEIGHT = 32;
	const int TOTAL_TILES = 10000;
	const int TOTAL_TILE_SPRITES = 16;

	//Kafelki - podloga
	const int TILE_PEBBLE_BROWN_01 = 0;
	const int TILE_PEBBLE_BROWN_02 = 1;
	const int TILE_PEBBLE_BROWN_03 = 2;
	const int TILE_PEBBLE_BROWN_04 = 3;
	const int TILE_PEBBLE_BROWN_05 = 4;
	const int TILE_PEBBLE_BROWN_06 = 5;
	const int TILE_PEBBLE_BROWN_07 = 6;
	const int TILE_PEBBLE_BROWN_08 = 7;
	const int TILE_PEBBLE_BROWN_09 = 8;

	//Kafelki - sciana
	const int TILE_WALL_DARK_BRICK_01 = 9;
	const int TILE_WALL_DARK_BRICK_02 = 10;
	const int TILE_WALL_DARK_BRICK_03 = 11;
	const int TILE_WALL_DARK_BRICK_04 = 12;
	const int TILE_WALL_DARK_BRICK_05 = 13;
	const int TILE_WALL_DARK_BRICK_06 = 14;
	const int TILE_WALL_DARK_BRICK_07 = 15;
}