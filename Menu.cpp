#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>

#include "LTexture.h"
#include "Constants.h"
#include "Menu.h"
#include "Globals.h"
#include "GameStates.h"
#include "GameStateFunctions.h"

using namespace std;

LTexture menuTexture;

Menu::Menu() {
	newGameButtonImage = NULL;
	loadGameButtonImage = NULL;
	exitGameButtonImage = NULL;
	titleScreenImage = NULL;

	newGameButtonWidth = 0;
	newGameButtonHeight = 0;
	loadGameButtonWidth = 0;
	loadGameButtonHeight = 0;
	exitGameButtonWidth = 0;
	exitGameButtonHeight = 0;
	loadMedia();
}

Menu::~Menu() {
	free();
}

bool Menu::loadMedia() {
	bool success = true;
	menuTexture.loadFromFile("title_screen.png");
	titleScreenImage = menuTexture.getTexture();
	if (titleScreenImage == NULL) {
		cout << "Nie wczytano obrazu PNG" << endl;
		success = false;
	}

	menuTexture.loadFromFile("new_game_button.png");
	newGameButtonImage = menuTexture.getTexture();
	if (newGameButtonImage == NULL) {
		cout << "Nie wczytano tekstury" << endl;
		success = false;
	}

	menuTexture.loadFromFile("load_game_button.png");
	loadGameButtonImage = menuTexture.getTexture();
	if (loadGameButtonImage == NULL) {
		cout << "Nie wczytano tekstury" << endl;
		success = false;
	}

	menuTexture.loadFromFile("exit_game_button.png");
	exitGameButtonImage = menuTexture.getTexture();
	if (exitGameButtonImage == NULL) {
		cout << "Nie wczytano tekstury" << endl;
		success = false;
	}

	return success;
}

void Menu::render() {
	screenViewport.x = 0;
	screenViewport.y = 0;
	screenViewport.w = Constants::SCREEN_W;
	screenViewport.h = Constants::SCREEN_H;
	SDL_RenderSetViewport(gRenderer, &screenViewport);
	SDL_RenderCopy(gRenderer, titleScreenImage, NULL, NULL);

	SDL_QueryTexture(newGameButtonImage, NULL, NULL, &newGameButtonWidth, &newGameButtonHeight);
	newGameButtonViewport.x = (Constants::SCREEN_W / 2) - (newGameButtonWidth / 2);
	newGameButtonViewport.y = (Constants::SCREEN_H / 2) - (newGameButtonHeight / 2);
	newGameButtonViewport.w = newGameButtonWidth;
	newGameButtonViewport.h = newGameButtonHeight;
	SDL_RenderSetViewport(gRenderer, &newGameButtonViewport);
	SDL_RenderCopy(gRenderer, newGameButtonImage, NULL, NULL);

	SDL_QueryTexture(loadGameButtonImage, NULL, NULL, &loadGameButtonWidth, &loadGameButtonHeight);
	loadGameButtonViewport.x = (Constants::SCREEN_W / 2) - (loadGameButtonWidth / 2);
	loadGameButtonViewport.y = (Constants::SCREEN_H / 2) + newGameButtonHeight;
	loadGameButtonViewport.w = loadGameButtonWidth;
	loadGameButtonViewport.h = loadGameButtonHeight;
	SDL_RenderSetViewport(gRenderer, &loadGameButtonViewport);
	SDL_RenderCopy(gRenderer, loadGameButtonImage, NULL, NULL);

	SDL_QueryTexture(exitGameButtonImage, NULL, NULL, &exitGameButtonWidth, &exitGameButtonHeight);
	exitGameButtonViewport.x = (Constants::SCREEN_W / 2) - (exitGameButtonWidth / 2);
	exitGameButtonViewport.y = (Constants::SCREEN_H / 2) + newGameButtonHeight + (loadGameButtonHeight * 1.5);
	exitGameButtonViewport.w = exitGameButtonWidth;
	exitGameButtonViewport.h = exitGameButtonHeight;
	SDL_RenderSetViewport(gRenderer, &exitGameButtonViewport);
	SDL_RenderCopy(gRenderer, exitGameButtonImage, NULL, NULL);
}

void Menu::free() {
	SDL_DestroyTexture(titleScreenImage);
	titleScreenImage = NULL;
	SDL_DestroyTexture(newGameButtonImage);
	newGameButtonImage = NULL;
	SDL_DestroyTexture(loadGameButtonImage);
	loadGameButtonImage = NULL;
	SDL_DestroyTexture(exitGameButtonImage);
	exitGameButtonImage = NULL;

	SDL_RenderSetViewport(gRenderer, &screenViewport);
}

void Menu::logic() {

}

void Menu::handle_event() {
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT) {
			set_next_state(STATE_EXIT);
		}
	}
	if (event.button.button == SDL_BUTTON_LEFT) {
		int x = event.button.x;
		int y = event.button.y;
		if ((x > exitGameButtonViewport.x) && (x < exitGameButtonViewport.x + exitGameButtonWidth) && (y > exitGameButtonViewport.y) && (y < exitGameButtonViewport.y + exitGameButtonHeight)) {
			set_next_state(STATE_EXIT);
		}
		if ((x > newGameButtonViewport.x) && (x < newGameButtonViewport.x + newGameButtonWidth) && (y > newGameButtonViewport.y) && (y < newGameButtonViewport.y + newGameButtonHeight)) {
			free();
			set_next_state(STATE_LEVEL);
		}
	}
}


