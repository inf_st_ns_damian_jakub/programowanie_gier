#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>

#include "Tile.h"
#include "LTexture.h"
#include "Constants.h"
#include "GameState.h"

class Map : public GameState{
public:
	Map();
	~Map();

	bool loadMedia(Tile* tiles[]);
	bool setTiles(Tile* tiles[]);
	void handle_event();
	void logic();
	void render();
	void free();
	SDL_Rect tileClips[Constants::TOTAL_TILE_SPRITES];
	Tile* tileSet[Constants::TOTAL_TILES];
private:
	int tileType;
	SDL_Texture* tileImage;
	LTexture mapTexture;

	LTexture uiTexture;
	SDL_Texture* uiImage;
	
	
};

