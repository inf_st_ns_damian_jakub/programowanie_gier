#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include<iostream>

using namespace std;

class LTexture {
public:
	LTexture();
	~LTexture();

	bool loadFromFile(string path);

	//Uwalnianie tekstury
	void free();

	//renderowanie tekstury w konkretnym miejscu
	void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	int getWidth();
	int getHeight();
	SDL_Texture* getTexture();

private:
	SDL_Texture* texture;

	int width;
	int height;

};
