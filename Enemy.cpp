#include "Enemy.h"
#include "Constants.h"
#include "Timer.h"
#include <random>

std::random_device randomDevice;
std::seed_seq seedSeq{ randomDevice() };
std::mt19937 engine{ seedSeq };
std::uniform_int_distribution<> direction(1, 4);

Timer wait;

Enemy::Enemy(int x, int y) {
	enemyBox.x = x;
	enemyBox.y = y;
	enemyBox.w = ENEMY_WIDTH;
	enemyBox.h = ENEMY_HEIGHT;
	loadMedia();
	wait.start();
}

Enemy::~Enemy() {

}
//Wczytanie grafiki
bool Enemy::loadMedia() {
	bool success = true;
	enemyTexture.loadFromFile("butterfly2.png");
	enemyImage = enemyTexture.getTexture();
	if (enemyImage == NULL) {
		cout << "Nie wczytano obrazu PNG" << endl;
		success = false;
	}
	return success;
}

SDL_Rect Enemy::getBox() {
	return enemyBox;
}

//Kolizja ze sciana. Funkcja sprawdza kazdy kafelek na mapie.
bool Enemy::wallCollision(SDL_Rect enemyBox, Tile* tiles[]) {
	for (int i = 0; i < Constants::TOTAL_TILES; i++) {
		if ((tiles[i]->getType() >= Constants::TILE_WALL_DARK_BRICK_01) && (tiles[i]->getType() <= Constants::TILE_WALL_DARK_BRICK_07)) {
			if (tile->checkCollision(enemyBox, tiles[i]->getBox())) {
				return true;
			}
		}
	}
	return false;
}

//Kolizja przeciwnika z graczem, wykorzystuje funkcje z Tile.cpp
bool Enemy::playerCollision(SDL_Rect enemyBox, SDL_Rect playerBox) {
	if (tile->checkCollision(enemyBox, playerBox)) {
		return true;
	}
	return false;
}

//Kolizja przeciwnik�w mi�dzy sob�, nie dzia�a, wy��czona
bool Enemy::enemyCollision(SDL_Rect enemyBox, std::vector<Enemy*> enemy_container) {
	std::vector<Enemy *>::iterator enemy_iterator;
	for (enemy_iterator = enemy_container.begin();
		enemy_iterator != enemy_container.end();
		++enemy_iterator) {
		if (tile->checkCollision(enemyBox, (*enemy_iterator)->getBox())) {
			cout << "kolizja" << endl;
			return true;
		}
	}
	return false;
}

//Poruszanie przeciwnikiem. W celu wywolania opoznienia, przeciwnik moze sie poruszac co sekunde i na ruch ma 50 milisekund.
void Enemy::move(Tile* tiles[], SDL_Rect playerBox, std::vector<Enemy*> enemy_container) {
	moveDirection = direction(engine);
	
	if (wait.getTicks() >= 1000) {
		switch (moveDirection) {
		case 1: enemyBox.x += 32;
			if ((enemyBox.x < 0) || (enemyBox.x + ENEMY_HEIGHT > Constants::LEVEL_H) || wallCollision(enemyBox, tiles) || playerCollision(enemyBox, playerBox)) {
				enemyBox.x -= 32;
			}
			break;
		case 2: enemyBox.x -= 32;
			if ((enemyBox.x < 0) || (enemyBox.x + ENEMY_HEIGHT > Constants::LEVEL_H) || wallCollision(enemyBox, tiles) || playerCollision(enemyBox, playerBox)) {
				enemyBox.x += 32;
			}
			break;
		case 3: enemyBox.y += 32;
			if ((enemyBox.y < 0) || (enemyBox.y + ENEMY_HEIGHT > Constants::LEVEL_H) || wallCollision(enemyBox, tiles) || playerCollision(enemyBox, playerBox)) {
				enemyBox.y -= 32;
			}
			break;
		case 4: enemyBox.y -= 32;
			if ((enemyBox.y < 0) || (enemyBox.y + ENEMY_HEIGHT > Constants::LEVEL_H) || wallCollision(enemyBox, tiles) || playerCollision(enemyBox, playerBox)) {
				enemyBox.y += 32;
			}
			break;
		}
		if (wait.getTicks() >= 1050) {
			wait.start();
		}
	}
}

void Enemy::render(SDL_Rect camera) {
	enemyTexture.render(enemyBox.x-camera.x, enemyBox.y-camera.y);
}