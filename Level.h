#pragma once
#include "GameState.h"
#include "Map.h"
#include "Player.h"

class Level : public GameState {
public:
	Level();
	~Level();
	void handle_event();
	void logic();
	void render();
private:
	Map map;
	Enemy *enemy;
	Player player;
};