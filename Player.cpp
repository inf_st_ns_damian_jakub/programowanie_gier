#include "Player.h"
#include "Globals.h"
#include "GameStateFunctions.h"
#include "GameStates.h"
#include "Constants.h"


Player::Player() {
	playerBox.x = 32;
	playerBox.y = 32;
	playerBox.w = PLAYER_WIDTH;
	playerBox.h = PLAYER_HEIGHT;
	mouseX = 0;
	mouseY = 0;
	

	speed = 0;
	click = false;
	loadMedia();
}

Player::~Player() {
	free();
}

void Player::free() {
	SDL_DestroyTexture(playerImage);
	playerImage = NULL;

}

bool Player::loadMedia() {
	bool success = true;
	playerTexture.loadFromFile("human_m.png");
	playerImage = playerTexture.getTexture();
	if (playerImage == NULL) {
		cout << "Nie wczytano obrazu PNG" << endl;
		success = false;
	}
	return success;
}

SDL_Rect Player::getBox() {
	return playerBox;
}

void Player::handleEvent(SDL_Event &event) {
	if (event.type == SDL_QUIT) {
		set_next_state(STATE_EXIT);
	}
	if (event.type == SDL_KEYDOWN && event.key.repeat == 0) {
		switch (event.key.keysym.sym) {
		case SDLK_UP: velY -= baseSpeed; break;
		case SDLK_DOWN: velY += baseSpeed; break;
		case SDLK_LEFT: velX -= baseSpeed; break;
		case SDLK_RIGHT: velX += baseSpeed; break;
		}
	}
	else if (event.type == SDL_KEYUP && event.key.repeat == 0) {
		switch (event.key.keysym.sym) {
		case SDLK_UP: velY += baseSpeed; break;
		case SDLK_DOWN: velY -= baseSpeed; break;
		case SDLK_LEFT: velX += baseSpeed; break;
		case SDLK_RIGHT: velX -= baseSpeed; break;
		}
	}
	if (event.button.button == SDL_BUTTON_LEFT) {
		click = true;
		mouseX = event.button.x;
		mouseY = event.button.y;

	}
}

//klikanie na przeciwnika, nie dzia�a
void Player::enemyClicked(std::vector<Enemy*> enemy_container) {
	collision.x = playerBox.x - 32;
	collision.y = playerBox.y - 32;
	collision.w = 96;
	collision.h = 96;
	for (enemy_iterator = enemy_container.begin();
		enemy_iterator != enemy_container.end();
		++enemy_iterator) {
		//if (tile->checkCollision(collision, (*enemy_iterator)->getBox())) {
			if (mouseX > (*enemy_iterator)->getBox().x && mouseX < (*enemy_iterator)->getBox().x + (*enemy_iterator)->getBox().w && mouseY >(*enemy_iterator)->getBox().y && mouseY < (*enemy_iterator)->getBox().y + (*enemy_iterator)->getBox().h) {
				cout << "clicked " << endl;
			}
		//}
	}
}

bool Player::wallCollision(SDL_Rect playerBox, Tile* tiles[]) {
	for (int i = 0; i < Constants::TOTAL_TILES; i++) {
		if ((tiles[i]->getType() >= Constants::TILE_WALL_DARK_BRICK_01) && (tiles[i]->getType() <= Constants::TILE_WALL_DARK_BRICK_07)) {
			if (tile->checkCollision(playerBox, tiles[i]->getBox())) {
				return true;
			}
		}
	}
	return false;
}

bool Player::enemyCollision(SDL_Rect playerBox, std::vector<Enemy*> enemy_container) {
	for (enemy_iterator = enemy_container.begin();
		enemy_iterator != enemy_container.end();
		++enemy_iterator) {
		if (tile->checkCollision(playerBox, (*enemy_iterator)->getBox())) {
				return true;
			}
	}
	return false;
}

void Player::move(Tile *tiles[], std::vector<Enemy*> enemy_container) {
	distanceX += velX;
	if (distanceX >= 32) {
		playerBox.x += 32;
		distanceX = 0;
	}
	else if (distanceX <= -32) {
		playerBox.x -= 32;
		distanceX = 0;
	}
	if ((playerBox.x < 0) || (playerBox.x + PLAYER_WIDTH > Constants::LEVEL_W) || wallCollision( playerBox, tiles) || enemyCollision(playerBox, enemy_container)) {
		playerBox.x -= velX;
	}
	distanceY += velY;
	if (distanceY >= 32) {
		playerBox.y += 32;
		distanceY = 0;
	}
	else if (distanceY <= -32) {
		playerBox.y -= 32;
		distanceY = 0;
	}

	if ((playerBox.y < 0) || (playerBox.y + PLAYER_HEIGHT > Constants::LEVEL_H) || wallCollision(playerBox, tiles) || enemyCollision(playerBox, enemy_container)) {
		playerBox.y -= velY;
	}
}

void Player::setCamera( SDL_Rect& camera) {
	camera.x = (playerBox.x + PLAYER_WIDTH / 2) - Constants::SCREEN_W / 2;
	camera.y = (playerBox.y + PLAYER_HEIGHT / 2) - Constants::SCREEN_H / 2;

	if (camera.x < -1) {
		camera.x = 0;
	}
	if (camera.y < -1) {
		camera.y = 0;
	}
	if (camera.x > Constants::LEVEL_W - camera.w) {
		camera.x = Constants::LEVEL_W - camera.w;
	}
	if (camera.y > Constants::LEVEL_H - camera.h) {
		camera.y = Constants::LEVEL_H - camera.h;
	}
}

void Player::render(SDL_Rect& camera) {
	playerTexture.render(playerBox.x - camera.x, playerBox.y - camera.y);
}