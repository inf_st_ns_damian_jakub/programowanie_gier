#pragma once
class GameState {
public:
	virtual void handle_event() = 0;
	virtual void logic() = 0;
	virtual void render() = 0;
	virtual ~GameState() {};
};
