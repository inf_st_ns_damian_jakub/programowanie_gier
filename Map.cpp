#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>

#include "Map.h"
#include "Globals.h"
#include "RandomMapGenerator.h"


using namespace std;

Map::Map() {
	randomMapGenerator();
	tileImage = NULL;
	loadMedia(tileSet);
}

Map::~Map() {
	free();
}

bool Map::loadMedia(Tile* tiles[]) {
	bool success = true;
	mapTexture.loadFromFile("dungeon_asset.png");
	tileImage = mapTexture.getTexture();
	if (tileImage == NULL) {
		cout << "Nie wczytano obrazu PNG" << endl;
		success = false;
	}

	uiTexture.loadFromFile("kwa.png");
	uiImage = uiTexture.getTexture();
	if (uiImage == NULL) {
		cout << "Nie wczytano obrazu PNG" << endl;
		success = false;
	}

	if (!setTiles(tiles)) {
		cout << "Nie mozna wczytac assetu!" << endl;
		success = false;
	}

	return success;
}

bool Map::setTiles(Tile* tiles[]) {
	bool tilesLoaded = true;

	int xOffset = 0, yOffset = 0;

	ifstream map("randomMap.map");
	if (map.fail()) {
		cout << "Nie mozna otworzyc mapy!" << endl;
		tilesLoaded = false;
	}
	else {
		for (int i = 0; i < Constants::TOTAL_TILES; i++) {
			int tileType = -1;
			map >> tileType;

			if (map.fail()) {
				cout << "Blad podczas wczytywania mapy" << endl;
				tilesLoaded = false;
				break;
			}

			if ((tileType >= 0) && (tileType < Constants::TOTAL_TILE_SPRITES)) {
				tiles[i] = new Tile(xOffset, yOffset, tileType);
			}
			else {
				cout << "Blad podczas wczytywania mapy" << endl;
				tilesLoaded = false;
				break;
			}

			xOffset += Constants::TILE_WIDTH;

			if (xOffset >= Constants::LEVEL_W) {
				xOffset = 0;
				yOffset += Constants::TILE_HEIGHT;
			}
		}

		if (tilesLoaded) {
			tileClips[Constants::TILE_PEBBLE_BROWN_01].x = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_01].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_01].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_01].h = Constants::TILE_HEIGHT;

			
			tileClips[Constants::TILE_PEBBLE_BROWN_02].x = 32;
			tileClips[Constants::TILE_PEBBLE_BROWN_02].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_02].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_02].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_PEBBLE_BROWN_03].x = 64;
			tileClips[Constants::TILE_PEBBLE_BROWN_03].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_03].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_03].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_PEBBLE_BROWN_04].x = 96;
			tileClips[Constants::TILE_PEBBLE_BROWN_04].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_04].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_04].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_PEBBLE_BROWN_05].x = 128;
			tileClips[Constants::TILE_PEBBLE_BROWN_05].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_05].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_05].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_PEBBLE_BROWN_06].x = 160;
			tileClips[Constants::TILE_PEBBLE_BROWN_06].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_06].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_06].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_PEBBLE_BROWN_07].x = 192;
			tileClips[Constants::TILE_PEBBLE_BROWN_07].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_07].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_07].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_PEBBLE_BROWN_08].x = 224;
			tileClips[Constants::TILE_PEBBLE_BROWN_08].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_08].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_08].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_PEBBLE_BROWN_09].x = 256;
			tileClips[Constants::TILE_PEBBLE_BROWN_09].y = 0;
			tileClips[Constants::TILE_PEBBLE_BROWN_09].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_PEBBLE_BROWN_09].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_WALL_DARK_BRICK_01].x = 0;
			tileClips[Constants::TILE_WALL_DARK_BRICK_01].y = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_01].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_WALL_DARK_BRICK_01].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_WALL_DARK_BRICK_02].x = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_02].y = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_02].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_WALL_DARK_BRICK_02].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_WALL_DARK_BRICK_03].x = 64;
			tileClips[Constants::TILE_WALL_DARK_BRICK_03].y = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_03].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_WALL_DARK_BRICK_03].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_WALL_DARK_BRICK_04].x = 96;
			tileClips[Constants::TILE_WALL_DARK_BRICK_04].y = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_04].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_WALL_DARK_BRICK_04].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_WALL_DARK_BRICK_05].x = 128;
			tileClips[Constants::TILE_WALL_DARK_BRICK_05].y = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_05].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_WALL_DARK_BRICK_05].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_WALL_DARK_BRICK_06].x = 160;
			tileClips[Constants::TILE_WALL_DARK_BRICK_06].y = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_06].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_WALL_DARK_BRICK_06].h = Constants::TILE_HEIGHT;

			tileClips[Constants::TILE_WALL_DARK_BRICK_07].x = 192;
			tileClips[Constants::TILE_WALL_DARK_BRICK_07].y = 32;
			tileClips[Constants::TILE_WALL_DARK_BRICK_07].w = Constants::TILE_WIDTH;
			tileClips[Constants::TILE_WALL_DARK_BRICK_07].h = Constants::TILE_HEIGHT;
			
		}
	}

	map.close();

	return tilesLoaded;
}

void Map::free() {
	SDL_DestroyTexture(tileImage);
	tileImage = NULL;
}

void Map::handle_event() {

}

void Map::logic() {

}

void Map::render() {
	for (int i = 0; i < Constants::TOTAL_TILES; i++) {
		tileSet[i]->render(camera, mapTexture, &tileClips[tileType]);
	}
}