#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>

#include "GameState.h"


class Menu : public GameState {
public:
	Menu();
	~Menu();
	bool loadMedia();
	void handle_event();
	void logic();
	void render();
	void free();

private:
	SDL_Rect screenViewport;

	SDL_Texture* newGameButtonImage;
	SDL_Texture* loadGameButtonImage;
	SDL_Texture* exitGameButtonImage;
	SDL_Texture* titleScreenImage;

	SDL_Rect exitGameButtonViewport;
	SDL_Rect loadGameButtonViewport;
	SDL_Rect newGameButtonViewport;
	int newGameButtonWidth, newGameButtonHeight;
	int loadGameButtonWidth, loadGameButtonHeight;
	int exitGameButtonWidth, exitGameButtonHeight;
};



