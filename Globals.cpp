#include "GameStates.h"
#include "Globals.h"
#include <cstdlib>
#include "Constants.h"

int stateID = STATE_NULL;
int nextState = STATE_NULL;

GameState* currentState = NULL;
SDL_Renderer* gRenderer = NULL;
SDL_Event event;
SDL_Rect camera = { 0, 0, Constants::SCREEN_W, Constants::SCREEN_H };