#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>

#include "LTexture.h"
#include "Globals.h"


class Tile {
	public:
		Tile(int x, int y, int tileType);

		void render(SDL_Rect& camera, LTexture texture, SDL_Rect tileClips[]);

		int getType();

		SDL_Rect getBox();

		bool checkCollision(SDL_Rect a, SDL_Rect b);

	private:
		SDL_Rect box;

		int type;
};
