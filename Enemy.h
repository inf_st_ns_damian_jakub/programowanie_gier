#pragma once
#include "LTexture.h"
#include "Tile.h"
#include <vector>


class Enemy {
public:
	const int ENEMY_WIDTH = 32;
	const int ENEMY_HEIGHT = 32;

	Enemy(int x, int y);
	~Enemy();
	void render(SDL_Rect camera);
	void move(Tile* tiles[], SDL_Rect playerBox, std::vector<Enemy*> enemy_container);
	bool loadMedia();
	void free();
	bool wallCollision(SDL_Rect enemyBox, Tile *tiles[]);
	bool playerCollision(SDL_Rect enemyBox, SDL_Rect playerBox);
	bool enemyCollision(SDL_Rect enemyBox, std::vector<Enemy*> enemy_container);
	SDL_Rect getBox();
private:
	Tile * tile;
	LTexture enemyTexture;
	SDL_Texture* enemyImage;
	SDL_Rect enemyBox;

	int velX, velY;
	int distanceX, distanceY;
	int moveDirection;
	std::vector<Enemy*> enemy_container;
};