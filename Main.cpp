#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>

using namespace std;

#include "MainFunctions.h"
#include "Map.h"
#include "Menu.h"
#include "GameStates.h"
#include "GameStateFunctions.h"
#include "Globals.h"
#include "Timer.h"

int main(int argc, char* args[]) {

	Timer fps;

	if (!initSDL()) {
		cout << "Blad przy inicjalizacji!" << endl;
	}
		stateID = STATE_MENU;
		currentState = new Menu();
				
		while (stateID != STATE_EXIT) {
			
			fps.start();

			currentState->handle_event();

			currentState->logic();
			change_state();

			currentState->render();
			SDL_RenderPresent(gRenderer);

			//Opoznienie aplikacji do wymaganej liczby FPS
			if (fps.getTicks() < 1000 / Constants::FPS) {
				SDL_Delay((1000 / Constants::FPS) - fps.getTicks());
			}
	}


	closeGame();

	return 0;
}