#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>

#include "Tile.h"
#include "Constants.h"

Tile::Tile(int x, int y, int tileType) {
	box.x = x;
	box.y = y;

	box.w = Constants::TILE_WIDTH;
	box.h = Constants::TILE_HEIGHT;

	type = tileType;
}

void Tile::render(SDL_Rect& camera, LTexture texture, SDL_Rect tileClips[]) {
	if (checkCollision(camera, box)) {
		texture.render(box.x - camera.x, box.y - camera.y, &tileClips[type]);
	}
}

int Tile::getType() {
	return type;
}

SDL_Rect Tile::getBox() {
	return box;
}

bool Tile::checkCollision(SDL_Rect a, SDL_Rect b) {
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	if (bottomA <= topB) {
		return false;
	}

	if (topA >= bottomB) {
		return false;
	}

	if (rightA <= leftB) {
		return false;
	}

	if (leftA >= rightB) {
		return false;
	}

	return true;
}