Technologie:  
C++, SDL, JSON  

Design:  
Widok 2D, kamera z góry.  
Single player.  
Ucieczka z labiryntu z elementami RPG.  
Poziomy, labirynty, możliwe, że generowane losowo, im wyższy poziom tym trudniejsze.  
Proste dźwięki i muzyka.  
Stałe 60 FPS.  
Detekcja kolizji metodą Bounding box  
Przełączanie między MENU i poziomami: State Machine  

UI:  
Zdrowie, mana itd. na dole ekranu, ekwipunek, inwentarz pod skrotem klawiszowym, osobne okienko.  


Sterowanie:  
- poruszanie się: WSAD  
- interakcja z obiektami: LPM  
- atak ( zaznaczanie przeciwnika ) PPM  
- akcja: E  
- inwentarz: I  
- ekwipunek: C  
- używanie umiejętności / czarów: klawisze 1-9  

Mapa:  
TiledMap, wielkosc kafelków: 32x32, wielkość mapy 100x100 kafelków lub 150x150 kafelków  
Poruszanie się po mapie po wyznaczonych kafelkach.  
Na mapie porozrzucane takie obiekty jak: skrzynie, beczki.  

Rozwój postaci:  
Zdobywanie poziomów: formuła: X = ( 25 * L * L ) - 25 * L gdzie X to doświadczenie, a L to poziom.  
Statystyki:  
- Siła: zwiększa zdrowie i obrażenia  
- Zręczność: zwiększa szybkość i szansę na trafienie  
- Inteligencja: zwiększa magię  
- Zdrowie: bazowo 100 + 10 co poziom + 10 za każdy punkt siły  
- Szybkość: bazowo 1 + 1 co poziom + 1 za każdy punkt zręczności. Każdy punkt zwiększa szybkość o 5%.  
- Magia: bazowo 50 + 5 co poziom + 5 za każdy punkt inteligencji.  
- Pancerz: bazowo 0, zależne od ekwipunku.  
- Szansa na trafienie: bazowo 50% + 1% za każdy punkt zręczności.  
- Obrażenia: bazowo 5 + 5 za każdy punkt siły + obrażenia broni - pancerz przeciwnika  

Ekwipunek:  
- Broń jednoręczna + tarcza lub broń dwuręczna  - broń: obrażenia, szybkość ataku,  tarcza: pancerz  ; wymagania: siła lub zreczność lub inteligencja  
- Hełm  - pancerz ( lekki, ciężki ). Dodaje zdrowie lub magię  
- Pancerz  - pancerz  ( lekki, ciężki ). Dodaje zdrowie lub magię  
- Nagawice  - pancerz  ( lekki, ciężki ). Dodaje zdrowie lub magię  
- Buty  - pancerz, szybkość  ( lekki, ciężki ). Dodaje zdrowie lub magię  lub szybkość  
- Dwa pierścienie  - + do statystyk  
- Naszyjnik  - + do statystyk  
- Przedmioty użytkowe - mikstury itp.  

Przedmioty:  
Zapisane w plikach .JSON 
Struktura ID przedmiotów: dwie pierwsze cyfry oznaczają rodzaj przedmiotu, dwie następne konkretny przedmiot  
- 1111: broń jednoręczna, sztylet  
- 1112: broń jednoręczna, zardzewiały miecz  
- 1211: broń dwuręczna  
- 1311: hełm  
- 1411: pancerz  
- 1511: nagawice  
- 1611: buty  
- 1711: pierścieie  
- 1811: naszyjnik  
- 1911: użytkowe  

Przeciwnicy:  
Zapisani w plikach .JSON  

Fabuła:  
Jedyne zadanie to wydostać się z labiryntu.  


Gameplay:  
Gracz zaczyna zamknięty w celi. Uwalnia go jakiś człowiek, ubrany tylko w szmaciane spodnie, ma przy sobie sztylet. Po uwolnieniu nas umiera, zabieramy mu przedmioty i próbujemy wydostać się z labiryntu.   Podczas poszukiwnaia wyjścia napotykamy przeciwników, znajdujemy przedmiot, uczymy się czarów. Na każdym poziomie są schody prowadzące w górę, co 5 poziom jest brama, której broni BOSS. Po pokonaniu go   możemy przejść dalej. Co 5 poziomów zmiana wyglądu mapy, trudniejsi przeciwnicy, lepsze przedmioty. Gdy gracz zginie zaczyna od nowa.  