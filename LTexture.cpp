#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>

#include"LTexture.h"
#include "Globals.h"

using namespace std;

LTexture::LTexture() {
	texture = NULL;
	width = 0;
	height = 0;
}

LTexture::~LTexture() {
	free();
}

bool LTexture::loadFromFile(string path) {

	//zwolnij instenijaca teksture przed wczytaniem nowej
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL) {
		cout << "Nie mozna wczytac obrazu " << path.c_str() << " SDL_Image Error: " << IMG_GetError() << endl;
	}
	else {
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
		if (newTexture == NULL) {
			cout << "Nie mozna utworzyc tekstury z " << path.c_str() << " SDL Error: " << SDL_GetError() << endl;
		}
		else {
			width = loadedSurface->w;
			height = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	texture = newTexture;
	return texture != NULL;
}

void LTexture::free() {
	if (texture != NULL) {
		//SDL_DestroyTexture(texture);
		texture = nullptr;
		width = 0;
		height = 0;
	}
}

void LTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip) {
	
	//obszar renderwowania tekstury
	SDL_Rect renderQuad = { x, y, width, height };
	
	if (clip != NULL) {
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopyEx(gRenderer, texture, clip, &renderQuad, angle, center, flip);
}

int LTexture::getWidth() {
	return width;
}

int LTexture::getHeight() {
	return height;
}

SDL_Texture* LTexture::getTexture() {
	return texture;
}