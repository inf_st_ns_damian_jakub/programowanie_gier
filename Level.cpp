#include "Level.h"
#include "GameStateFunctions.h"
#include <random>

std::random_device r;
std::seed_seq seed{ r() };
std::mt19937 eng{ seed };
std::uniform_int_distribution<> dist(1, 99);

std::vector<Enemy *> enemy_container;
std::vector<Enemy *>::iterator enemy_iterator;

Level::Level() {
	//Wypelnienie wektora przeciwnikami o losowej pozycji na mapie.
	for (int i = 0; i < 100; i++) {
		Enemy *enemy = new Enemy(32 * dist(eng), 32 * dist(eng));
		enemy_container.push_back(enemy);
	}
}

Level::~Level() {

}

void Level::handle_event() {
	while (SDL_PollEvent(&event) != 0) {
		if (event.type == SDL_QUIT) {
			set_next_state(STATE_EXIT);
		}
		map.handle_event();
		player.handleEvent(event);
	}
}

void Level::logic() {
	player.move(map.tileSet, enemy_container);
	player.setCamera(camera);
	player.enemyClicked(enemy_container);
	for (enemy_iterator = enemy_container.begin();
		enemy_iterator != enemy_container.end();
		++enemy_iterator) {
		(*enemy_iterator)->move(map.tileSet, player.getBox(), enemy_container);
	}
}

void Level::render() {
	map.render();
	for (enemy_iterator = enemy_container.begin();
		enemy_iterator != enemy_container.end();
		++enemy_iterator) {
		
		(*enemy_iterator)->render(camera);
	}
	player.render(camera);
}