#include "GameStateFunctions.h"
#include "Menu.h"
#include "Level.h"
#include "GameStates.h"
#include "GameState.h"
#include "Globals.h"

//Ustawienie nowego stanu aplikacji
void set_next_state(int newState) {
	if (nextState != STATE_EXIT) {
		nextState = newState;
	}
}

//Zmiana stanu aplikacji
void change_state() {
	if (nextState != STATE_NULL) {
		if (nextState != STATE_EXIT) {
			delete currentState;
		}
			switch (nextState) {
			case STATE_MENU:
				currentState = new Menu();
				break;

			case STATE_LEVEL:
				currentState = new Level();
				break;
			}
			stateID = nextState;
			nextState = STATE_NULL;
	}
}