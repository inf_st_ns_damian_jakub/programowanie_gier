Ogólny plan tworzenia gry:  

1. Utworzenie menu głównego (title screen)  
    1.1 Opcje: Nowa gra, Wczytaj grę, Wyjdź  [DONE]  
    1.2 [Opcjonalnie] Intro przed menu głównym  
2. Tworzenie mapy  
    2.1 Stworzenie mapy na sztywno  [DONE]  
    2.2 Utworzenie State Machines  [DONE]  
    2.3 [Opcjonalnie] Ekran ładowania podczas tworzenia mapy  
    2.4 Dodanie obiektów do mapy  
    2.5 Dodanie kamery  [DONE]  
    2.6 Losowe generowanie mapy  
3. Dodanie gracza  
    3.1 Utworzenie gracza na mapie  [DONE]  
    3.2 Kolizje z otoczeniem (poruszanie się po kafelkach) [DONE]  
    3.3 Animacja ruchu  
    3.4 Dodanie UI ze statystykami i ekwipunkiem  
4. Dodanie przedmiotów  
    4.1 Dodanie przedmiotów leżących na mapie  
    4.2 Używanie przedmiotów przez gracza  
    4.3 Dodanie przedmiotów w obiektach na mapie  
5. Dodanie przeciwników  
    5.1 Spawn pointy dla przeciwników  
    5.2 AI dla przeciwników  
    5.3 Przedmioty z przeciwników  
    5.4 Interakcja gracza z przeciwnikiem  
    5.5 Zdobywanie doświadczenia przez gracza po pokonaniu przeciwnika  
    5.6 Zdobywanie poziomów przez gracza  
    5.7 Przydzielanie statystyk przez gracza  
6. Dźwięk  
    6.1 Dodanie muzyki  
    6.2 Dodanie efektów dźwiękowych  
7. Animacje  
    7.1 Dodanie różnych animacji  
8. Zapisywanie / wczytywanie stanu gry  
9. Poprawa błędów / optymalizacja  